package es.xoubin.historico.domain;

import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Access(AccessType.FIELD)
@Table( name = "Equipos" )
public class Dispositivo implements Serializable {
    @Transient
    private SimpleStringProperty id = new SimpleStringProperty();
    @Transient
    private SimpleStringProperty idl = new SimpleStringProperty();
    @Transient
    private SimpleStringProperty descripcion = new SimpleStringProperty();

    public Dispositivo() {
    }
           
    @Id
    @Access(AccessType.PROPERTY)
    @Column(name="id")
    public String getId() {
        return id.get();
    }
    
    public void setId(String id) {
        this.id = new SimpleStringProperty(id);
    }
    
    public SimpleStringProperty idProperty(){
        return id;
    }
    
    @Column(name="descripcion")
    @Access(AccessType.PROPERTY)
    public final String getDescripcion() {
        return descripcion.get();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = new SimpleStringProperty(descripcion);
    }
    
    public SimpleStringProperty descripcionProperty(){
        return descripcion;
    }
    
    @Access(AccessType.PROPERTY)
    public String getIdl() {
        return idl.get();
    }
    
    public void setIdl(String idl) {
        this.idl = new SimpleStringProperty(idl);
    }
    
    public SimpleStringProperty idlProperty(){
        return idl;
    }
   
    public Dispositivo(String ID, String IDL, String Descripcion) {
        this.id = new SimpleStringProperty(ID);
        this.idl = new SimpleStringProperty(IDL);
        this.descripcion = new SimpleStringProperty(Descripcion);
    }
}
