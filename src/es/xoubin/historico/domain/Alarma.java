package es.xoubin.historico.domain;

import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Access(AccessType.FIELD)
@Table( name = "Logger" )
public class Alarma implements Serializable{
    @Transient
    private SimpleStringProperty entity= new SimpleStringProperty();
    @Transient
    private SimpleStringProperty fecha = new SimpleStringProperty();
    @Transient
    private SimpleStringProperty tipo= new SimpleStringProperty();
    @Transient
    private SimpleStringProperty mensaje= new SimpleStringProperty();
       
    public Alarma() {
    }
    
    @Access(AccessType.PROPERTY)
    public final String getTipo() {
        return tipo.get();
    }

    public void setTipo(String tipo) {
        this.tipo = new SimpleStringProperty(tipo);
    }
    
    public SimpleStringProperty tipoProperty(){
        return tipo;
    }
    
    @Id
    @Column(name="Entity")
    @Access(AccessType.PROPERTY)
    public String getEntity() {
        return entity.get();
    }
    
    public void setEntity(String entity) {
        this.entity = new SimpleStringProperty(entity);
    }
    
    public SimpleStringProperty entityProperty(){
        return entity;
    }
    
    @Access(AccessType.PROPERTY)
    public String getMensaje() {
        return mensaje.get();
    }
    
    public void setMensaje(String mensaje) {
        this.mensaje = new SimpleStringProperty(mensaje);
    }
    
    public SimpleStringProperty mensajeProperty(){
        return mensaje;
    }
    
    @Access(AccessType.PROPERTY)
    public String getFecha() {
        return fecha.get();
    }
    
    public void setFecha(String fecha) {
        this.fecha = new SimpleStringProperty(fecha);
    }
    
    public SimpleStringProperty fechaProperty(){
        return fecha;
    }
   
    public Alarma(String ENTITY, String tipo, String mensaje, String fecha) {
        this.entity = new SimpleStringProperty(ENTITY);
        this.tipo = new SimpleStringProperty(tipo);
        this.mensaje = new SimpleStringProperty(mensaje);
        this.fecha = new SimpleStringProperty(fecha);
    }
}
