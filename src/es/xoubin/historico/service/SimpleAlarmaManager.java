package es.xoubin.historico.service;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import es.xoubin.historico.config.HibernateUtil;
import es.xoubin.historico.domain.Alarma;
import java.util.Iterator;
import org.hibernate.Query;
import org.hibernate.Session;

public class SimpleAlarmaManager implements AlarmaManager{
	
	private static final long serialVersionUID = 1L;
	
	public ObservableList<Alarma> getAlarmasList(String id)
	{
            ObservableList<Alarma> lista = FXCollections.observableArrayList();

            Session session = HibernateUtil.getSessionFactoryLogger().getCurrentSession();
            session.beginTransaction ();
            try{
                Query query = session.createQuery("select A.fecha, A.entity, A.mensaje, A.tipo from Alarma A where A.entity = '" + id + "' order by A.fecha desc");
                Iterator<Object[]> list = query.iterate();
                while(list.hasNext()) {
                    Object[] a = list.next();
                    //System.out.println(a[0]);
                    String idele = a[1].toString();
                    String tipo = a[3].toString();
                    String mensaje = a[2].toString();
                    String fechaString = a[0].toString().substring(0, a[0].toString().length()-2);
                    lista.add(new Alarma(idele, tipo, mensaje, fechaString));
                }
            }
            catch(Exception ex){
                    System.out.println(ex.toString());
            }
            session.close();
            return lista;
	}
	
	public Alarma getAlarma(Integer id)
	{
		return null;
		
	}
	
}
