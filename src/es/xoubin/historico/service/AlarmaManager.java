package es.xoubin.historico.service;

import java.io.Serializable;
import java.util.List;

import es.xoubin.historico.domain.Alarma;

public interface AlarmaManager extends Serializable {
	
	public List<Alarma> getAlarmasList(String idl);
	public Alarma getAlarma(Integer id);
	
}

