package es.xoubin.historico.service;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import es.xoubin.historico.config.HibernateUtil;
import es.xoubin.historico.domain.Dispositivo;
import java.util.List;
import org.hibernate.Session;

public class SimpleDispositivoManager implements DispositivoManager{
	
	private static final long serialVersionUID = 1L;
	
	public ObservableList<Dispositivo> getDispositivoList(){
                ObservableList<Dispositivo> lista = FXCollections.observableArrayList();
                Session session = HibernateUtil.getSessionFactoryEquipos().getCurrentSession();
                session.beginTransaction ();
                
                List<Dispositivo> list = (List<Dispositivo>) session.createQuery("SELECT D FROM Dispositivo D order by D.id").list();                
		try{
                    for (Dispositivo d : list) {
                        String nombre = d.getDescripcion();
                        nombre=nombre.replace("{", "");
                        nombre=nombre.replace("}", "");
                        String id=d.getId();
                        String idl=d.getId();
                        idl=idl.substring(4);
                        lista.add(new Dispositivo(id, idl, nombre));
                    }
		}
		catch(Exception ex){
			System.out.println(ex.toString());
		}
                session.close();
		return lista;
	}
	
	public Dispositivo getDispositivo(Integer id)
	{
		return null;
		
	}
	
}
