package es.xoubin.historico.service;

import java.io.Serializable;
import java.util.List;

import es.xoubin.historico.domain.Dispositivo;

public interface DispositivoManager extends Serializable {
	
	public List<Dispositivo> getDispositivoList();
	public Dispositivo getDispositivo(Integer id);
	
}

