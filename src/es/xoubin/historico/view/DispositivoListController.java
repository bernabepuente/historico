package es.xoubin.historico.view;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import es.xoubin.historico.domain.Alarma;
import es.xoubin.historico.domain.Dispositivo;
import es.xoubin.historico.service.SimpleAlarmaManager;
import es.xoubin.historico.service.SimpleDispositivoManager;
import javafx.scene.control.cell.PropertyValueFactory;

public class DispositivoListController{
	
    private SimpleDispositivoManager dispositivoManager;
    ObservableList<Dispositivo> dispositivos;
    
    private SimpleAlarmaManager alarmaManager;
    ObservableList<Alarma> alarmas;
    
    @FXML
    TableView<Dispositivo> tablaDispositivos = new TableView<>();  
    @FXML
    TableView<Alarma> tablaAlarmas = new TableView<>();   
    @FXML
    TableColumn<Dispositivo, String> clID;
    @FXML
    TableColumn<Dispositivo, String> clIDL;
    @FXML
    TableColumn<Dispositivo, String> clDescripcion;
    @FXML
    TableColumn<Alarma, String> clIdAlarma;
    @FXML
    TableColumn<Alarma, String> clIDLAlarma;
    @FXML
    TableColumn<Alarma, LocalDateTime> clFecha;
    @FXML
    TableColumn<Alarma, String> clTipo;
    @FXML
    TableColumn<Alarma, String> clMensaje;

    public DispositivoListController() {
    }
	
    @FXML
    private void initialize() {
    	dispositivoManager=new SimpleDispositivoManager();	
    	tablaDispositivos.setEditable(false);
    	
    	alarmaManager=new SimpleAlarmaManager();	
    	tablaAlarmas.setEditable(false);
    	
        //formatearColumnaFecha();
        inicializarTablaDispositivos();
        
        // listener para detectar cambio en la seleccion del dispositivo y cargar
        //las alarmas correspondientes.
        tablaDispositivos.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> cargarAlarmas(newValue));
    }
    
    //Formateamos la columna Fecha
    private void formatearColumnaFecha(){
    	DateTimeFormatter myDateFormatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        clFecha.setCellFactory(column -> {return new TableCell<Alarma, LocalDateTime>() {
                    @Override
                    protected void updateItem(LocalDateTime item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                            setText(null);
                            setStyle("");
                    } else {
                            // Format date.
                            setText(myDateFormatter.format(item));
                    }
                }
            };
        });
    }
    
    private void inicializarTablaDispositivos() {
            // El metodo setCellValueFactory asocia las celdas de la tabla a los getters del modelo
            clID.setCellValueFactory(new PropertyValueFactory<>("id"));
            clIDL.setCellValueFactory(new PropertyValueFactory<>("idl"));
            clDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));

            dispositivos = FXCollections.observableArrayList();
            tablaDispositivos.setEditable(true);
            tablaDispositivos.setItems(dispositivos);

            try {
                    dispositivos = dispositivoManager.getDispositivoList();
            } catch (Exception e) {
                    e.getMessage();
            }
            tablaDispositivos.setItems(dispositivos);
    }

    private void cargarAlarmas(Dispositivo disp){
            System.out.println("IDL " + disp.getId());

            clFecha.setCellValueFactory(new PropertyValueFactory<>("fecha"));
            clTipo.setCellValueFactory(new PropertyValueFactory<>("tipo"));
            clMensaje.setCellValueFactory(new PropertyValueFactory<>("mensaje"));

            alarmas = FXCollections.observableArrayList();
            tablaAlarmas.setEditable(true);
            tablaAlarmas.setItems(alarmas);

            try {
                    String id = disp.idProperty().get();
                    alarmas = alarmaManager.getAlarmasList(id);
            } catch (Exception e) {
                    e.getMessage();
            }

            tablaAlarmas.setItems(alarmas);
    }
}




