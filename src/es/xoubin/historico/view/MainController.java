package es.xoubin.historico.view;

import javafx.fxml.FXML;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class MainController {

	@FXML
	private MenuBar menu;
	@FXML
	private Menu archivo;
	@FXML
	private MenuItem salir;
	
	public MainController(){
		menu = new MenuBar();
		archivo = new Menu();
		salir = new MenuItem();
	}
	
	@FXML
	public void exit() {
		System.exit(0);
	}
}
