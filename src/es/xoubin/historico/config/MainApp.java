package es.xoubin.historico.config;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.hibernate.Criteria;
import org.hibernate.Session;
import es.xoubin.historico.domain.Dispositivo;
import java.util.List;
import org.hibernate.Query;

public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    
    @Override
    public void start(Stage primaryStage) {	
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Historico de Alarmas");
        initRootLayout();
        showDispositivoList();
    }
	
	/**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/es/xoubin/historico/view/Main.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the person overview inside the root layout.
     */
    public void showDispositivoList() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/es/xoubin/historico/view/DispositivoList.fxml"));
            AnchorPane dispositivoList = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(dispositivoList);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactoryEquipos().getCurrentSession();
        session.beginTransaction ();
        
        Query q = session.createQuery("SELECT d FROM Dispositivo d");
        List resultList = q.list();
        System.out.println ("Dispositivo encontradas:" + resultList.size ());
        
//        Criteria criteria = session.createCriteria ( Dispositivo.class );
//        System.out.println ("Dispositivo encontradas:" + criteria.list ().size ());
        launch(args);
    }
}
