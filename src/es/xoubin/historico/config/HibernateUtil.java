/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.xoubin.historico.config;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author operador
 */
public class HibernateUtil {
    private static final SessionFactory sessionFactoryEquipos;
    private static final SessionFactory sessionFactoryLogger;
   
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            sessionFactoryEquipos = new Configuration().configure("hibernateEquipos.cfg.xml").buildSessionFactory();
            sessionFactoryLogger = new Configuration().configure("hibernateLogger.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactoryEquipos() {
        return sessionFactoryEquipos;
    }
    
    public static SessionFactory getSessionFactoryLogger() {
        return sessionFactoryLogger;
    }
}
