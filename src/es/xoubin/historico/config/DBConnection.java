package es.xoubin.historico.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBConnection {

	Connection conexionNSC=null;
	Connection conexionGest=null;
	
	public java.sql.Connection getConexionNSC() {
		return conexionNSC;
	}

	public void setConexionNSC(Connection conexionNSC) {
		this.conexionNSC = conexionNSC;
	}

	public DBConnection(){
		  try {
			  Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	          conexionNSC=DriverManager.getConnection("jdbc:sqlserver://172.30.26.2:1433;databaseName=NSC_Equipos","sa","Etra-3000");
	          conexionGest=DriverManager.getConnection("jdbc:sqlserver://172.30.26.2:1433;databaseName=GestHist","sa","Etra-3000");
		  } 
		  catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
	}
	  
	public java.sql.Connection getConexionGest() {
		return conexionGest;
	}

	public void setConexionGest(Connection conexionGest) {
		this.conexionGest = conexionGest;
	}
}
